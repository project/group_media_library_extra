<?php

declare(strict_types = 1);

namespace Drupal\group_media_library_extra_groupmedia\Plugin\GroupMediaLibraryExtra\MediaItemsSource;

use Drupal\group_media_library_extra\MediaItemsSource\MediaItemsSourceBase;
use Drupal\group_media_library_extra\MediaItemsSource\MediaItemsSourceInterface;
use Drupal\media_library\MediaLibraryState;
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\ViewExecutable;

/**
 * Media items without group plugin instance.
 *
 * @GroupMediaLibraryMediaItemsSource(
 *   id = "media_items_without_group",
 *   label = @Translation("Media items without group"),
 *   description = @Translation("Use the media items the do not belong to any group."),
 *   contexts = {
 *     "no_group",
 *   },
 * )
 */
class MediaItemsWithoutGroup extends MediaItemsSourceBase implements MediaItemsSourceInterface {

  /**
   * {@inheritdoc}
   */
  public function query(ViewExecutable $view, QueryPluginBase $query): void {
    // This plugin filter out the media items that belong to group.
    $state = MediaLibraryState::fromRequest($view->getRequest());
    $definition = [
      'table' => 'group_relationship_field_data',
      'field' => 'entity_id',
      'left_table' => 'media_field_data',
      'left_field' => 'mid',
      'type' => 'LEFT',
      'extra' => [
        ['field' => 'plugin_id', 'value' => 'group_media:' . $state->getSelectedTypeId()],
      ],
    ];
    $query->addRelationship('grfd', $this->joinManager->createInstance('standard', $definition), 'media');
    $query->addWhereExpression(0, 'grfd.gid IS NULL');
  }

}
