<?php

declare(strict_types = 1);

namespace Drupal\group_media_library_extra_groupmedia\Plugin\GroupMediaLibraryExtra\MediaItemsSource;

use Drupal\group\Entity\Group;
use Drupal\group_media_library\GroupMediaLibraryState;
use Drupal\group_media_library_extra\MediaItemsSource\MediaItemsSourceBase;
use Drupal\group_media_library_extra\MediaItemsSource\MediaItemsSourceInterface;
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\ViewExecutable;

/**
 * Group's media items plugin instance.
 *
 * @GroupMediaLibraryMediaItemsSource(
 *   id = "group_media_items",
 *   label = @Translation("Group's media items"),
 *   description = @Translation("Use the media items in the group. The plugin won't be applied on media types that has no relation installed."),
 *   contexts = {
 *     "existing_group",
 *   },
 * )
 */
class GroupMediaItems extends MediaItemsSourceBase implements MediaItemsSourceInterface {

  /**
   * {@inheritdoc}
   */
  public function query(ViewExecutable $view, QueryPluginBase $query): void {
    $state = GroupMediaLibraryState::fromRequest($view->getRequest());

    // Add a relationship to the corresponding enabled media relation
    // plugin.
    if ($state->getGroupMediaRelationPluginId()) {
      $definition = [
        'table' => 'group_relationship_field_data',
        'field' => 'entity_id',
        'left_table' => 'media_field_data',
        'left_field' => 'mid',
        'type' => 'INNER',
        'extra' => [
          ['field' => 'plugin_id', 'value' => $state->getGroupMediaRelationPluginId()],
        ],
      ];

      $query->addRelationship('grfd', $this->joinManager->createInstance('standard', $definition), 'media');
      $query->addWhereExpression('AND', 'grfd.gid = :gid', ['gid' => $state->getGroupId()]);
    }
  }

}
