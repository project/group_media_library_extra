<?php

/**
 * @file
 * Provide views runtime hooks for group_media_library.
 */

declare(strict_types = 1);

use Drupal\group_media_library_extra\MediaLibraryWidgetViewsQueryAlter;
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\ViewExecutable;

/**
 * Implements hook_views_query_alter().
 */
function group_media_library_extra_views_query_alter(ViewExecutable $view, QueryPluginBase $query): void {
  if ($view->id() === 'media_library' && str_starts_with($view->current_display, 'widget')) {
    \Drupal::service('class_resolver')
      ->getInstanceFromDefinition(MediaLibraryWidgetViewsQueryAlter::class)
      ->alterQuery($view, $query);
  }
}
