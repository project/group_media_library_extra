CONTENTS OF THIS FILE
---------------------

* Introduction
* Features
* How to use

INTRODUCTION
------------

Provides extra functionality and features to [Group Media Library](https://www.drupal.org/project/group_media_library) module.


FEATURES
--------

* Media library view:
Plugable solution to alter the media items list in the media library. It allows
you to select the source of media items for each group type and also for content
that is being created from outside group context (global content).
The module comes with some predefined media items sources:
  - Own media items: Use it when you want the users to use their own media items.
  - Group's media items: It requires groupmedia module. This is useful if you
    want to allow users to use media items that belong to the group itself only.
  - Media without group: It requires groupmedia module. It is active when outside
    of group context. This is useful if you want to prevent users from using media
    items that belongs to group from being used outside the group itself.

You still can create your own plugin if none of the above plugins meets your
requirement.


HOW TO USE
----------
-  For setting up the media items source for a group type, visit the "Media library"
   tab in the group type.
-  For setting up the media items source for global content, visit the Groups >
   Settings > Media Library Extra Settings.
