<?php

declare(strict_types = 1);

namespace Drupal\group_media_library_extra;

use Drupal\group\Entity\GroupTypeInterface;
use Drupal\group_media_library_extra\MediaItemsSource\MediaItemsSourceInterface;

/**
 * Interface for group media library group type service.
 */
interface GroupMediaLibraryGroupTypeInterface {

  /**
   * Set the setting for media items source.
   *
   * @param \Drupal\group\Entity\GroupTypeInterface $group_type
   *   Group type.
   * @param array $setting
   *   The setting.
   */
  public function setMediaItemsSourceSetting(GroupTypeInterface $group_type, array $setting): void;

  /**
   * Get the setting for media items source.
   *
   * @param \Drupal\group\Entity\GroupTypeInterface $group_type
   *   Group type.
   *
   * @return array|null
   *   The setting for media items source if exits, null otherwise.
   */
  public function getMediaItemsSourceSetting(GroupTypeInterface $group_type): ?array;

  /**
   * Get a plugin for media items source.
   *
   * @param \Drupal\group\Entity\GroupTypeInterface $group_type
   *   Group type.
   * @param string $context
   *   The context, possible values are: existing_group, new_group, no_group.
   *
   * @return array|null
   *   The plugin for media items source if exits, null otherwise.
   */
  public function getMediaItemsSourcePlugin(GroupTypeInterface $group_type, string $context): ?MediaItemsSourceInterface;

  /**
   * Get the plugin id for media items source for existing group.
   *
   * @param \Drupal\group\Entity\GroupTypeInterface $group_type
   *   Group type.
   *
   * @return string|null
   *   The plugin id for media items source if exits, null otherwise.
   */
  public function getMediaItemsSourceExistingGroupPluginId(GroupTypeInterface $group_type): ?string;

  /**
   * Get the plugin id for media items source for ew group.
   *
   * @param \Drupal\group\Entity\GroupTypeInterface $group_type
   *   Group type.
   *
   * @return string|null
   *   The plugin id for media items source if exits, null otherwise.
   */
  public function getMediaItemsSourceNewGroupPluginId(GroupTypeInterface $group_type): ?string;

}
