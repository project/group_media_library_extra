<?php

declare(strict_types = 1);

namespace Drupal\group_media_library_extra;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupType;
use Drupal\group_media_library\GroupMediaLibraryState;
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\Plugin\ViewsHandlerManager;
use Drupal\views\ViewExecutable;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class for altering views queries.
 */
class MediaLibraryWidgetViewsQueryAlter implements ContainerInjectionInterface {

  /**
   * The Views join plugin manager.
   *
   * @var \Drupal\views\Plugin\ViewsHandlerManager
   */
  protected $joinManager;

  /**
   * The media library group type service.
   *
   * @var \Drupal\group_media_library_extra\GroupMediaLibraryGroupTypeInterface
   */
  private $MediaLibraryGroupType;

  /**
   * The group media library extra settings service.
   *
   * @var \Drupal\group_media_library_extra\GroupMediaLibrarySettingsInterface
   */
  private $GroupMediaLibrarySettings;

  /**
   * Constructs a GroupToGroupRelationship object.
   *
   * @param \Drupal\views\Plugin\ViewsHandlerManager $join_manager
   *   The views plugin join manager.
   * @param \Drupal\group_media_library_extra\GroupMediaLibraryGroupTypeInterface $media_library_group_type
   *   The media library group type service.
   * @param \Drupal\group_media_library_extra\GroupMediaLibrarySettingsInterface $group_media_library_settings
   *   The group media library extra settings service.
   */
  public function __construct(ViewsHandlerManager $join_manager, GroupMediaLibraryGroupTypeInterface $media_library_group_type, GroupMediaLibrarySettingsInterface $group_media_library_settings) {
    $this->joinManager = $join_manager;
    $this->MediaLibraryGroupType = $media_library_group_type;
    $this->GroupMediaLibrarySettings = $group_media_library_settings;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('plugin.manager.views.join'),
      $container->get('group_media_library_extra.group_type'),
      $container->get('group_media_library_extra.settings'),
    );
  }

  /**
   * Implements a hook bridge for hook_views_query_alter().
   *
   * Alter core's media library view to apply views behavior plugins on the
   * view.
   *
   * @see group_media_library_extra_views_query_alter()
   */
  public function alterQuery(ViewExecutable $view, QueryPluginBase $query): void {
    // If there is a group media library state, this means we are in a group
    // context.
    if ($state = GroupMediaLibraryState::fromRequest($view->getRequest())) {
      // Filter media items in case of existing group.
      if (($gid = $state->getGroupId()) && ($group = Group::load($gid))) {
        $plugin = $this->MediaLibraryGroupType->getMediaItemsSourcePlugin($group->getGroupType(), 'existing_group');
      }
      else {
        // Filter media items in case of new group.
        $group_type = GroupType::load($state->getGroupTypeId());
        $plugin = $this->MediaLibraryGroupType->getMediaItemsSourcePlugin($group_type, 'new_group');
      }
      $plugin?->query($view, $query);
    }
    else {
      // When not in a group context, the default behavior of the media library
      // is displaying all media items in the system.
      // Here we let the no_group plugin to alter the query of the views in
      // order to change the list of the available media items.
      $this->GroupMediaLibrarySettings->getMediaItemsSourceNoGroupPlugin()?->query($view, $query);
    }
  }

}
