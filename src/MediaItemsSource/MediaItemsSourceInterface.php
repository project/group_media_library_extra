<?php

declare(strict_types = 1);

namespace Drupal\group_media_library_extra\MediaItemsSource;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\ViewExecutable;

/**
 * Interface for media items source plugin instance.
 */
interface MediaItemsSourceInterface extends PluginInspectionInterface, ContainerFactoryPluginInterface {

  /**
   * Alter the media library query.
   */
  public function query(ViewExecutable $view, QueryPluginBase $query): void;

}
