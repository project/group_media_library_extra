<?php

declare(strict_types = 1);

namespace Drupal\group_media_library_extra\MediaItemsSource;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\group_media_library_extra\Annotation\GroupMediaLibraryMediaItemsSource;

/**
 * Manages media items source plugins.
 *
 * @see \Drupal\group_media_library_extra\Annotation\GroupMediaLibraryMediaItemsSource
 * @see \Drupal\group_media_library_extra\MediaItemsSource\MediaItemsSourceInterface
 * @see \Drupal\group_media_library_extra\MediaItemsSource\MediaItemsSourceBase
 * @see plugin_api
 */
class MediaItemsSourceManager extends DefaultPluginManager {

  /**
   * Constructs a new MediaItemsSourceManager.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/GroupMediaLibraryExtra/MediaItemsSource', $namespaces, $module_handler, MediaItemsSourceInterface::class, GroupMediaLibraryMediaItemsSource::class);
    $this->alterInfo('group_media_library_media_items_source_info');
    $this->setCacheBackend($cache_backend, 'group_media_library_media_items_source_plugins');
  }

  /**
   * {@inheritdoc}
   */
  public function processDefinition(&$definition, $plugin_id) {
    parent::processDefinition($definition, $plugin_id);

    // If there is no contexts, this is a broken definition.
    if (empty($definition['contexts'])) {
      throw new PluginException(sprintf('Group media library extra - Media items source plugin (%s) definition must include "contexts".', $plugin_id));
    }
    // If in valid context value, this is a broken definition.
    foreach ($definition['contexts'] as $context) {
      if (!in_array($context, ['existing_group', 'new_group', 'no_group'])) {
        throw new PluginException(sprintf('The context (%s) for Group media library extra - Media items source plugin (%s) definition is not valid. Possible contexts are (existing_group, new_group, no_group)', $context, $plugin_id));
      }
    }
  }

}
