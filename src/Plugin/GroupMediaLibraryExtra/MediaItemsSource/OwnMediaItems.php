<?php

declare(strict_types = 1);

namespace Drupal\group_media_library_extra\Plugin\GroupMediaLibraryExtra\MediaItemsSource;

use Drupal\Core\Session\AccountInterface;
use Drupal\group_media_library_extra\MediaItemsSource\MediaItemsSourceBase;
use Drupal\group_media_library_extra\MediaItemsSource\MediaItemsSourceInterface;
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\Plugin\ViewsHandlerManager;
use Drupal\views\ViewExecutable;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Own media items plugin instance.
 *
 * @GroupMediaLibraryMediaItemsSource(
 *   id = "own_media_items",
 *   label = @Translation("Own media items"),
 *   description = @Translation("Use own media items."),
 *   contexts = {
 *     "new_group",
 *     "existing_group",
 *   },
 * )
 */
class OwnMediaItems extends MediaItemsSourceBase implements MediaItemsSourceInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * MediaItemsSourceBase constructor.
   *
   * @param array $configuration
   *   Plugin configuration.
   * @param string $plugin_id
   *   Plugin id.
   * @param mixed $plugin_definition
   *   Plugin definition.
   * @param \Drupal\views\Plugin\ViewsHandlerManager $join_manager
   *   The views plugin join manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ViewsHandlerManager $join_manager, AccountInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $join_manager);
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.views.join'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query(ViewExecutable $view, QueryPluginBase $query): void {
    $query->addWhereExpression('AND', 'media_field_data.uid = :uid', ['uid' => $this->currentUser->id()]);
  }

}
