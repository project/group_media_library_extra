<?php

declare(strict_types = 1);

namespace Drupal\group_media_library_extra;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\group_media_library_extra\MediaItemsSource\MediaItemsSourceInterface;

/**
 * Interface for group media library settings service.
 */
interface GroupMediaLibrarySettingsInterface {

  /**
   * Get all group media library extra settings.
   *
   * @return ImmutableConfig
   *   The setting for group media library extra.
   */
  public function getAllSetting(): ImmutableConfig;

  /**
   * Get the media items source settings.
   *
   * @return array|null
   *   The media items source settings, null otherwise.
   */
  public function getMediaItemsSourceSettings(): ?array;

  /**
   * Get the no_group plugin for media items source.
   *
   * @return array|null
   *   The plugin for no_group media items source if exits, null otherwise.
   */
  public function getMediaItemsSourceNoGroupPlugin(): ?MediaItemsSourceInterface;

}
