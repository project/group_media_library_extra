<?php

declare(strict_types = 1);

namespace Drupal\group_media_library_extra\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a group media library media items source annotation object.
 *
 * @see plugin_api
 *
 * @Annotation
 */
class GroupMediaLibraryMediaItemsSource extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * A brief description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description = '';

  /**
   * The contexts of the source.
   *
   * Possible values are: existing_group, new_group, no_group.
   *
   * @var array
   */
  public $contexts = [];

}
