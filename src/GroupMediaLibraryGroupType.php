<?php

declare(strict_types = 1);

namespace Drupal\group_media_library_extra;

use Drupal\group\Entity\GroupTypeInterface;
use Drupal\group_media_library_extra\MediaItemsSource\MediaItemsSourceInterface;
use Drupal\group_media_library_extra\MediaItemsSource\MediaItemsSourceManager;

/**
 * Service class for getting group media library settings for a group type.
 */
class GroupMediaLibraryGroupType implements GroupMediaLibraryGroupTypeInterface {

  /**
   * The plugin manager for group media library media items source.
   *
   * @var \Drupal\group_media_library_extra\MediaItemsSource\MediaItemsSourceManager
   */
  private $MediaItemsSourceManager;

  /**
   * Constructs a new GroupMediaLibraryGroupType object.
   *
   * @param \Drupal\group_media_library_extra\MediaItemsSource\MediaItemsSourceManager $media_items_source_manager
   *   The plugin manager for group media library media items source.
   */
  public function __construct(MediaItemsSourceManager $media_items_source_manager) {
    $this->MediaItemsSourceManager = $media_items_source_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function setMediaItemsSourceSetting(GroupTypeInterface $group_type, array $setting): void {
    $group_type->setThirdPartySetting('group_media_library_extra', 'media_items_source', $setting)->save();
  }

  /**
   * {@inheritdoc}
   */
  public function getMediaItemsSourceSetting(GroupTypeInterface $group_type): ?array {
    return $group_type->getThirdPartySetting('group_media_library_extra', 'media_items_source');
  }

  /**
   * {@inheritdoc}
   */
  public function getMediaItemsSourcePlugin(GroupTypeInterface $group_type, string $context): ?MediaItemsSourceInterface {
    $plugin_id = $this->getMediaItemsSourceSetting($group_type)[$context] ?? NULL;
    return $plugin_id ? $this->MediaItemsSourceManager->createInstance($plugin_id) : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getMediaItemsSourceExistingGroupPluginId(GroupTypeInterface $group_type): ?string {
    return $this->getMediaItemsSourceSetting($group_type)['existing_group'] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getMediaItemsSourceNewGroupPluginId(GroupTypeInterface $group_type): ?string {
    return $this->getMediaItemsSourceSetting($group_type)['new_group'] ?? NULL;
  }

}
