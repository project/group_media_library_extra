<?php

declare(strict_types = 1);

namespace Drupal\group_media_library_extra\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\GroupTypeInterface;
use Drupal\group_media_library_extra\GroupMediaLibraryGroupTypeInterface;
use Drupal\group_media_library_extra\MediaItemsSource\MediaItemsSourceManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form to configure the media library for a group type.
 */
class GroupTypeMediaLibraryExtraSettingsForm extends FormBase {

  /**
   * The plugin manager for group media library media items source.
   *
   * @var \Drupal\group_media_library_extra\MediaItemsSource\MediaItemsSourceManager
   */
  private $MediaItemsSourceManager;

  /**
   * The media library group type service.
   *
   * @var \Drupal\group_media_library_extra\GroupMediaLibraryGroupTypeInterface
   */
  private $MediaLibraryGroupType;

  /**
   * Constructs GroupTypeMediaLibraryExtraSettingsForm.
   *
   * @param \Drupal\group_media_library_extra\MediaItemsSource\MediaItemsSourceManager $media_items_source_manager
   *   The plugin manager for group media library media items source.
   * @param \Drupal\group_media_library_extra\GroupMediaLibraryGroupTypeInterface $media_library_group_type
   *   The media library group type service.
   */
  public function __construct(MediaItemsSourceManager $media_items_source_manager, GroupMediaLibraryGroupTypeInterface $media_library_group_type) {
    $this->MediaItemsSourceManager = $media_items_source_manager;
    $this->MediaLibraryGroupType = $media_library_group_type;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('plugin.manager.group_media_library_extra.media_items_source'),
      $container->get('group_media_library_extra.group_type')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'group_media_library_extra_group_type_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, GroupTypeInterface $group_type = NULL): array {
    // Store the group type in the form state.
    $form_state->set('group_type', $group_type);

    $definitions = $this->MediaItemsSourceManager->getDefinitions();
    $media_items_source_plugins = [];
    foreach ($definitions as $plugin_id => $definition) {
      foreach ($definition['contexts'] as $context) {
        $media_items_source_plugins[$context][$plugin_id] =
          [
            'plugin_id' => $plugin_id,
            'label' => $definition['label'],
            'description' => $definition['description'],
          ];
      }
    }
    $form['media_items_source'] = [
      '#type' => 'details',
      '#title' => $this->t('Media items source settings'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];

    // Existing group setting.
    if (!empty($media_items_source_plugins['existing_group'])) {
      $form['media_items_source']['existing_group'] = [
        '#title' => $this->t('Existing group plugin'),
        '#type' => 'radios',
        '#options' => ['' => $this->t('None')] + array_column($media_items_source_plugins['existing_group'], 'label', 'plugin_id'),
        '#default_value' => $this->MediaLibraryGroupType->getMediaItemsSourceExistingGroupPluginId($group_type),
        '#description' => $this->t('Select the media items source for existing group.'),
      ];
      foreach ($media_items_source_plugins['existing_group'] as $option) {
        $form['media_items_source']['existing_group'][$option['plugin_id']]['#description'] = $option['description'];
      }
    }

    // New group setting.
    if (!empty($media_items_source_plugins['new_group'])) {
      $form['media_items_source']['new_group'] = [
        '#title' => $this->t('New group plugin'),
        '#type' => 'radios',
        '#options' => ['' => $this->t('None')] + array_column($media_items_source_plugins['new_group'], 'label', 'plugin_id'),
        '#default_value' => $this->MediaLibraryGroupType->getMediaItemsSourceNewGroupPluginId($group_type),
        '#description' => $this->t('Select the media items source for creating new group.'),
      ];
      foreach ($media_items_source_plugins['new_group'] as $option) {
        $form['media_items_source']['new_group'][$option['plugin_id']]['#description'] = $option['description'];
      }
    }

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    if ($group_type = $form_state->get('group_type')) {
      $this->MediaLibraryGroupType->setMediaItemsSourceSetting($group_type, array_filter($form_state->getValue('media_items_source')));
    }
  }

}
