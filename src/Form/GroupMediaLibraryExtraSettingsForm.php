<?php

declare(strict_types = 1);

namespace Drupal\group_media_library_extra\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\GroupTypeInterface;
use Drupal\group_media_library_extra\MediaItemsSource\MediaItemsSourceManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form to configure the group media library extra.
 */
class GroupMediaLibraryExtraSettingsForm extends ConfigFormBase {

  /**
   * The plugin manager for group media library media items source.
   *
   * @var \Drupal\group_media_library_extra\MediaItemsSource\MediaItemsSourceManager
   */
  private $MediaItemsSourceManager;

  /**
   * Constructs GroupMediaLibraryExtraSettingsForm.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\group_media_library_extra\MediaItemsSource\MediaItemsSourceManager $media_items_source_manager
   *   The plugin manager for group media library media items source.
   */
  public function __construct(ConfigFactoryInterface $config_factory, MediaItemsSourceManager $media_items_source_manager) {
    parent::__construct($config_factory);
    $this->MediaItemsSourceManager = $media_items_source_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.group_media_library_extra.media_items_source')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['group_media_library_extra.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'group_media_library_extra_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, GroupTypeInterface $group_type = NULL): array {
    $config = $this->config('group_media_library_extra.settings');

    $definitions = $this->MediaItemsSourceManager->getDefinitions();
    $media_items_source_plugins = [];
    foreach ($definitions as $plugin_id => $definition) {
      foreach ($definition['contexts'] as $context) {
        if ($context === 'no_group') {
          $media_items_source_plugins[$context][$plugin_id] =
            [
              'plugin_id' => $plugin_id,
              'label' => $definition['label'],
              'description' => $definition['description'],
            ];
        }
      }
    }
    $form['media_items_source'] = [
      '#type' => 'details',
      '#title' => $this->t('Media items source settings - outside group context'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];

    // No group setting.
    if (!empty($media_items_source_plugins['no_group'])) {
      $form['media_items_source']['no_group'] = [
        '#title' => $this->t('No group plugin'),
        '#type' => 'radios',
        '#options' => ['' => $this->t('None')] + array_column($media_items_source_plugins['no_group'], 'label', 'plugin_id'),
        '#default_value' => $config->get('media_items_source.no_group'),
        '#description' => $this->t('Select the media items source when creating global content (outside of group context).'),
      ];
      foreach ($media_items_source_plugins['no_group'] as $option) {
        $form['media_items_source']['no_group'][$option['plugin_id']]['#description'] = $option['description'];
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('group_media_library_extra.settings')
      ->set('media_items_source', $form_state->getValue('media_items_source'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
