<?php

declare(strict_types = 1);

namespace Drupal\group_media_library_extra;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\group_media_library_extra\MediaItemsSource\MediaItemsSourceInterface;
use Drupal\group_media_library_extra\MediaItemsSource\MediaItemsSourceManager;

/**
 * Service class for getting group media library settings.
 */
class GroupMediaLibrarySettings implements GroupMediaLibrarySettingsInterface {

  /**
   * The plugin manager for group media library media items source.
   *
   * @var \Drupal\group_media_library_extra\MediaItemsSource\MediaItemsSourceManager
   */
  private $MediaItemsSourceManager;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new GroupMediaLibrarySettings object.
   *
   * @param \Drupal\group_media_library_extra\MediaItemsSource\MediaItemsSourceManager $media_items_source_manager
   *   The plugin manager for group media library media items source.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(MediaItemsSourceManager $media_items_source_manager, ConfigFactoryInterface $config_factory) {
    $this->MediaItemsSourceManager = $media_items_source_manager;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function getAllSetting(): ImmutableConfig {
    return $this->configFactory->get('group_media_library_extra.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getMediaItemsSourceSettings(): ?array {
    return $this->getAllSetting()->get('media_items_source');
  }

  /**
   * {@inheritdoc}
   */
  public function getMediaItemsSourceNoGroupPlugin(): ?MediaItemsSourceInterface {
    $plugin_id = $this->getMediaItemsSourceSettings()['no_group'] ?? NULL;
    return $plugin_id ? $this->MediaItemsSourceManager->createInstance($plugin_id) : NULL;
  }

}
